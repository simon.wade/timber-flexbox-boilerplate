<?php
$context = Timber::context();
$timber_post = new Timber\Post();

// $args = [
// 	'post_type'				=>		'case-studies',
// 	'number_of_posts'		=>		1,
// 	'order'					=>		'rand',
// ];

wp_reset_postdata();

// $args_buckets = [
// 	'post_type'				=>		'page',
//     'post_parent'    		=> 		$post->post_parent ? $post->post_parent : get_queried_object_id(),
//     'order'          		=> 		'ASC',
//     'orderby'        		=> 		'menu_order'
// ];

$context['post'] 			= $timber_post;
$context['posts'] 			= new Timber\PostQuery();
// $context['case_study'] 		= Timber::get_posts( $args );
// $context['buckets'] 		= new Timber\PostQuery( $args_buckets );

Timber::render( [ 'page-'.$timber_post->post_name.'.twig', 'page.twig' ], $context );
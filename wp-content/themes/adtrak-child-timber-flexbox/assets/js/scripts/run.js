// binds $ to jquery, requires you to write strict code. Will fail validation if it doesn't match requirements.
(function($) {
    "use strict";

	// add all of your code within here, not above or below
	$(function() {



		// --------------------------------------------------------------------------------------------------
		// Back to top
		// --------------------------------------------------------------------------------------------------
		$("#back-top").hide();
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 300) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});
		});
		$("#back-top").click(function() {
			$("html, body").animate({
			scrollTop: $("body").offset().top
			}, 750);
		});

		// --------------------------------------------------------------------------------------------------
		// Mobile Menu
		// --------------------------------------------------------------------------------------------------
		// Copy primary and secondary menus to .mob-nav element
		var mobNav = document.querySelector('.mob-nav .scroll-container');

		var copyPrimaryMenu = document.querySelector('#navigation .menu-primary').cloneNode(true);
		mobNav.appendChild(copyPrimaryMenu);

		if($('#menu-secondary-menu').length) {
			var copySecondaryMenu = document.querySelector('#menu-secondary-menu').cloneNode(true);
			mobNav.appendChild(copySecondaryMenu);
		}

		// Add Close Icon element
		$( "<div class='mob-nav-close'><svg class='icon'><use xlink:href='"+adtrak.theme_url+"/assets/images/icons-sprite.svg#icon-close'></use></svg></div>" ).insertAfter( ".mob-nav .scroll-container" );

		// Add dropdown arrow to links with sub-menus
	    $( "<span class='sub-arrow'><svg class='icon'><use xlink:href='"+adtrak.theme_url+"/assets/images/icons-sprite.svg#icon-angle-down'></use></svg></div></span>" ).insertAfter( ".mob-nav .menu-item-has-children > a" );

	    // Show sub-menu when dropdown arrow is clicked
	    $('.sub-arrow').click(function() {
	    	$(this).toggleClass('active');
	    	$(this).prev('a').toggleClass('active');
	    	$(this).next('.sub-menu').slideToggle();
	    	$(this).children().toggleClass('fa-angle-down').toggleClass('fa-angle-up');
	    });

	    // Add underlay element after mobile nav
	    $( "<div class='mob-nav-underlay'></div>" ).insertAfter( ".mob-nav" );

	    // Show underlay and fix the body scroll when menu button is clicked
	    $('.menu-btn').click(function() {
	    	$('.mob-nav,.mob-nav-underlay').addClass('mob-nav--active');
	    	$('body').addClass('fixed');
	    });

	    // Hide menu when close icon or underlay is clicked
	    $('.mob-nav-underlay,.mob-nav-close').click(function() {
	    	$('.mob-nav,.mob-nav-underlay').removeClass('mob-nav--active');
	    	$('body').removeClass('fixed');
	    });


        // --------------------------------------------------------------------------------------------------
        // Adds active class to all links
        // --------------------------------------------------------------------------------------------------

        $("a").filter(function(){
        	return this.href == location.href.replace(/#.*/, "");
        }).addClass("active");

		
        // --------------------------------------------------------------------------------------------------
		// Ninja Forms event tracking | https://www.chrisains.com/seo/tracking-ninja-form-submissions-with-google-analytics-jquery/
		// --------------------------------------------------------------------------------------------------
        jQuery( document ).on( 'nfFormReady', function() {
        	nfRadio.channel('forms').on('submit:response', function(form) {
                gtag('event', 'conversion', {'event_category': form.data.settings.title,'event_action': 'Send Form','event_label': 'Successful '+form.data.settings.title+' Enquiry'});
        		console.log(form.data.settings.title + ' successfully submitted');

        	});
        });

 		
		// --------------------------------------------------------------------------------------------------
		// Unwrap Ninja forms
		// --------------------------------------------------------------------------------------------------

    	$(window).load(function(){
	        $('.nf-field-container').unwrap('nf-field');
	    });


	   	// --------------------------------------------------------------------------------------------------
		// Hero Video
		// --------------------------------------------------------------------------------------------------

		if(window.matchMedia("(min-width: 1200px)").matches && adtrak.hero_video) {
		    var heroVideoSrc = adtrak.hero_video; // Store the source
		    var heroEl = document.querySelector('.hero'); // Store access to the hero image
		    var videoEl = document.createElement('video'); // Create a video element
		    var videoSrcEl = document.createElement('source'); // Create a source element
		    videoSrcEl.setAttribute('src', heroVideoSrc); // Set the src to the correct value
		    videoEl.setAttribute('autoplay', true); // Set the autoplay attribute
		    videoEl.setAttribute('loop', true); // Set the autoplay attribute
		  	videoEl.setAttribute('muted', true); // Set the autoplay attribute
		    videoEl.classList.add('hero__video'); // Add classes to the video element
		    videoEl.appendChild(videoSrcEl); // Append the video source to the video element
		    heroEl.appendChild(videoEl); // Append the whole thing to the hero
		    videoEl.muted = true;
		    videoEl.controls = false;
			videoEl.autoplay = true;

		}

		// --------------------------------------------------------------------------------------------------
		// SVG Sprites
		// --------------------------------------------------------------------------------------------------


		var iconList = "<svg class='icon'><use xlink:href='"+adtrak.theme_url+"/assets/images/icons-sprite.svg#icon-caret-right'></use></svg>";
		$(iconList).prependTo( ".copy ul li");

		var iconDownArrow = "<svg class='icon'><use xlink:href='"+adtrak.theme_url+"/assets/images/icons-sprite.svg#icon-angle-down'></use></svg>";
		$(iconDownArrow).prependTo( "#navigation .menu-item-has-children");	

        
	});

}(jQuery));






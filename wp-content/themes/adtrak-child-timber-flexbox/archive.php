<?php
$context = Timber::context();
$timber_post = new Timber\Post();

$context['post'] = $timber_post;
$context['posts'] = new Timber\PostQuery(['orderby' => 'rand', 'post_type' => 'case-studies', 'posts_per_page' => -1]);
$context['categories'] = Timber::get_terms('case_study_categories', array( 'hide_empty' => true ));

$context['archive_title'] = post_type_archive_title( '', false );

Timber::render( [ 'archive.twig' ], $context );
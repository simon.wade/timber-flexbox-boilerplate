<?php
$context = Timber::context();
$timber_post = new Timber\Post();

// $args = [
// 	'post_type'				=>		'case-studies',
// 	'posts_per_page'		=>		1,
// 	'orderby'				=>		'rand',
// 	'order'					=>		'DESC',
// ];

$context['post'] 			= $timber_post;
$context['posts'] 			= new Timber\PostQuery();
// $context['case_study'] 		= new Timber\PostQuery( $args );

Timber::render( [ 'front-page.twig' ], $context );
// We need to require our plugins at the top of the file
const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const browserSync = require('browser-sync');
const concat = require('gulp-concat');
const purgecss = require('gulp-purgecss');

/*************************************************
 * GOING FURTHER WITH BROWSERSYNC
 *************************************************/

// Create a Gulp task to use BrowserSync
// You may notice this is written slightly different to others.
// That's because this is using just the node package rather than a gulp plugin
// As such we need to tell Gulp when the plugin is complete hence the done() action at the end
// Setup the BrowserSync plugin


const server = browserSync.create(); 

const reloadTask = function(done) {
	server.init({
		proxy: "http://timber-flexbox-boilerplate.vm"
	})
	done();
}
/*************************************************
 * BASIC GULP TASKS
 *************************************************/

// Sass Compilation
const sass_compile = function() {
    return src('./assets/scss/*.scss')
        .pipe(sass({
            outputStyle: "compressed",
            precision: 10
        }))

        .pipe(purgecss({
            content: ['templates/**/*.twig'],
            folders: ['./assets/scss/*'], 
            extensions: ['twig'],
            fontFace: true,
            whitelist: [
                    'btn', 'wp-notification', 'closed', 'open', 'mob-nav-underlay', 'mob-nav--active', 'mob-nav-close', 'sub-arrow'
                ],
            whitelistPatterns: [
                    /(menu-)/gim, /(wp-notification-)/gim, /(icon-)/gim, /(btn-)/gim 
                ], 
            keyframes: true,
            whitelistPatternsChildren: [
                    /(menu-)/gim, /(wp-notification-)/gim, /(icon-)/gim, /(btn-)/gim
                ]
        }))

        .pipe(dest('./dist'))
        .pipe(server.stream())
}

// Javascript compilation
const js_compile = function() {
    return src(['./assets/js/scripts/run.js'])
		.pipe(concat("site.js"))
		.pipe(uglify())
		.pipe(dest('./dist'));
}

// Ensuring Gulp can see the tasks by exporting them out
exports.js_compile = js_compile;
exports.sass_compile = sass_compile;

/*************************************************
 * EXPANDING BASIC TASKS WITH WATCH TASKS
 *************************************************/

// We don't want to have to run the tasks manually every time, so we create a 
// watch task to watch over the files for changes, and run the corresponding
// task instead

const watch_everything = function() {
	watch('./assets/scss/**/*.scss', sass_compile);
    watch(['./assets/js/scripts/run.js'], js_compile);
}
exports.watch_everything = watch_everything;
// Creating the tasks to run in the console
// exports.sass_compile = sass_compile;
// Run gulp --tasks to see an available list of tasks in the terminal
//// gulp watch_everything
// gulp js_compile
// gulp sass_compile



exports.serve_website = series(reloadTask, watch_everything);

// Default task that you can run just by calling gulp
exports.default = watch_everything;


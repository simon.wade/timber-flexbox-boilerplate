<?php
$context = Timber::context();
$timber_post = new Timber\Post(get_option('page_for_posts'));

$context['post'] = $timber_post;
$context['posts'] = new Timber\PostQuery();

Timber::render( [ 'home.twig' ], $context );
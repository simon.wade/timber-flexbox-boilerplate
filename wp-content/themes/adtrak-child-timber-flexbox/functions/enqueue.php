<?php

add_action('wp_enqueue_scripts', function () {
	wp_register_script('production', get_stylesheet_directory_uri() . '/dist/site.js', ['jquery'], '', true);
	wp_enqueue_style( 'master', get_stylesheet_directory_uri() . '/dist/main.css', [], null, 'all' );
	 wp_enqueue_script('svgxuse', get_stylesheet_directory_uri() . '/assets/js/scripts/svgxuse.js', [], '', true );

	wp_localize_script( 'production', 'adtrak', [
		'site_url' => get_home_url(),
		'hero_video' => get_field('video_hero'),
		'theme_url' => get_stylesheet_directory_uri()
	] );
	
	wp_enqueue_script('production');
	
});

function my_deregister_styles() {
	wp_deregister_style('adtrak-cookie'); // Disable separate stylesheet for cookie notice (styles can be found in footer.scss)
	wp_deregister_style('wp-block-library'); // Gutenberg related stylesheet
}
add_action('wp_print_styles', 'my_deregister_styles', 100);
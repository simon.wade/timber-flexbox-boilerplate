<?php
// Get twig/timber setup
require_once( __DIR__ . '/timber.inc.php' );

// $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
// $dotenv->load();

// All functions are seperated down into seperate files in the functions folder for better maintainability.
$files = ( defined( 'WP_DEBUG' ) AND WP_DEBUG ) ? glob( __DIR__ . '/functions/*.php', GLOB_ERR ) : glob( __DIR__ . '/functions/*.php' );
foreach ( $files as $file ) : include $file; endforeach;